;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                             ;;
;;;                Rules to map tokens to words    	                        ;;
;;;                                                                             ;;
;;;  Copyright (c) 2006, Priti Patil, janabhaaratii, C-DAC, Mumbai              ;;
;;;                     <prithisd@cdacmumbai.in>, <prithisd@gmail.com>          ;;
;;;                                                                             ;;
;;;  Copyright (c) 2005, Chaitanya Kamisetty <chaitanya@atc.tcs.co.in>    	;;
;;;                                                                             ;;
;;;  This program is a part of festival-mr.					;;
;;;  										;;
;;;  festival-mr is free software; you can redistribute it and/or modify        ;;
;;;  it under the terms of the GNU General Public License as published by	;;
;;;  the Free Software Foundation; either version 2 of the License, or		;;
;;;  (at your option) any later version.					;;
;;;										;;
;;;  This program is distributed in the hope that it will be useful,		;;
;;;  but WITHOUT ANY WARRANTY; without even the implied warranty of		;;
;;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the		;;
;;;  GNU General Public License for more details.				;;
;;;										;;
;;;  You should have received a copy of the GNU General Public License		;;
;;;  along with this program; if not, write to the Free Software		;;
;;;  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA  ;;
;;;										;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Abbreviations when followed by a dot and a space ex: "Dr. Hello"
;; or when followed by one of the symbols listed in marathi_abbr_markers_table
;; are replaced with their full forms
(defvar marathi_dotted_abbr_list
  '(
    ;; Time
    ("ता" "घंटे") ;;  Hours
    ("मि" "मिनीट") ;;  Minutes
    ;; Designations
    ("डॉ" "डाक्टर") ;;  Doctor 
    ("चि" "चिरंजीवी") ;;  A title used for men 
    ("सौ" "सौभाग्यवती") ;; A title used for women 
    ("कु" "कुमारी") ;;  A title used for bachlorettes
    ;; Morning, Afternoon, Evening, Night - There are four zones instead of just AM PM
    ("" "") ;;  Early morning
    ("" "") ;;  Morning
    ("" "") ;;  Afternoon
    ("" "") ;;  Evening
    ("रा" "रात") ;;  Night
    ;; Rough equivalents of AM and PM from Sanskrit
    ("पू" "पूर्वान्ह:") ;; poorvaahna
    ("अ" "अपरान्ह:") ;; apraahna
))

;; Above list contains items that will be followed by a dot or other character
;; Here the list contains abbreviations that are not followed by any puncuation
;; but need to be expanded anyway
(defvar marathi_abbr_list
  '(
    ;; Weekdays (as defined by the POSIX hi_IN locale
    ("रवि" "रविवार") ;;  Sunday
    ("सोम" "सोमवार") ;;  Monday
    ("मंगल" "मंगलवार") ;;  Tuesday
    ("बुध" "बुधवार") ;;  Wednesday
    ("बृ" "बृहस्पतिवार") ;;  Thirsday
    ("शुक्र" "शुकरवार") ;;  Friday
    ("शनि" "शनिवार") ;;  Saturday
    ("1/2"  "आधा") ;;  Half
    ("1/4" "पाव") ;;  Quarter
))

;; If these words follow a currency, then we need to 
;; change the order of pronounciation for this word,
;; number and the currency symbol
(defvar marathi_denominations_list
  '(
   ("बिलिअन्स") ("बिलिअन")   
   ("अरब")
   ("करोड़")
   ("लाख") 
   ("हजार")
  ("सौ")
))
		
;; This list the symbols that need to be spoken out
;; If a symbol is found in marathi_currency_list it should have
;; an expansion in this list. Other than currency list, all other 
;; symbols are the last to be processed in the tokenising process
(defvar marathi_common_symbols_table
  '(
    ("#" "हॅश") ;;  Hash
    ("$" "डॉलर") ;;  Dollor
    ("%" "टक्का") ;;  Percentage
    ("&" "और") ;;  And
    ("*" "") ;;  Multiply
    ("+" "अधिक") ;;  Plus
    ("," "") ;;  Comma
    ("-" "") ;; Minus
    ("." "") ;; eat up the dot
    ("/" "") ;;  By
    (":" "कोलन") ;;  Colon
    (";" "") ;; eat up the semi colon
    ("<" ("से" "छोटा")) ;;  Less Than
    ("=" "बराबर") ;;  Equal to
    (">" ("से" "बडा")) ;;  Greater than
    ("?" "") ;; eat up the question mark
    ("@" "अॅट") ;;  At
    ("^" "") ;; eat up Caret
    ("_" ("अंडर" "स्कोर")) ;;  Under Score
    ("`" ("बॅक" "कोट")) ;;  Back Quote
    ("~" "टिल्डा") ;; Tilde
    ("©" "कॉपीराईट") ;;  Copyright Sign
    ("®" "रजिस्टर्ड") ;;  Registered Sign
    ("£" "पाऊंड") ;;  Pound
    ("€" "युरो") ;; Euro
    ("¥" "येन") ;; Yen
    ("¢" "सेन्ट") ;; Cent
    ("।" "")	;; Eat up Devanagari Danda
    ("॥" "")	;; Eat up Devanagari Double Danda
    ;;ensure digits are always at the end of the table
    ;;so that they will be used as separators after other symbols have been considered
    ("0" "शून्य") ;; Zero
    ("1" "एक") ;;  One
    ("2" "दो") ;; Two
    ("3" "तीन") ;; Three
    ("4" "चार") ;;  Four
    ("5" "पाँच") ;; Five
    ("6" "छः") ;; Six
    ("7" "सात") ;; Seven
    ("8" "आठ") ;; Eight
    ("9" "नौ") ;; Nine
))

;; These are the additional characters allowed by the string
;; cleanup function
(defvar marathi_supplementary_char_list
  '("£" "€" "¥" "©" "®" "¢" "।" "॥"))

;; The text is checked if it contains these items almost at the
;; begining of the processing and full forms will be substitued 
;; by taking the full form from common symbols table
(defvar marathi_currency_list
  '( "$" "£" "€" "¥"))

;; If the items in dotted_abbr_list is followed by these symbols
;; Then they are conisered a match and expanded otherwise
;; they are not a match. Dot will be parsed by festival specially
;; so there is also a special handling for it in the code
(defvar marathi_abbr_markers_table
   '( ("." "") ("।" "") ("॥" "") ))
 
;; Marathi numbers are converted to westren numerals during the
;; string cleanup
(defvar marathi_numbers_table
  '(
    ("०" "0") ;; Zero
    ("१" "1") ;; One
    ("२" "2") ;; Two
    ("३" "3") ;; Three
    ("४" "4") ;; Four
    ("५" "5") ;; Five
    ("६" "6") ;; Six
    ("७" "7") ;; Seven
    ("८" "8") ;; Eight
    ("९" "9") ;; Nine
))

;; This is main function of this file. This gets called
;; from outside. the function is expected to return a string
;; of words that is the normalised form of the string
;; The output string is later sent to letter to sound rules
(define (marathi_token_to_words token name)
    (flatten (marathi_token_to_words_ token name))
)

;; A way to convert a given char to its ascii value
;; Could not find any better/direct way to do it
(defvar ascii_char_hash (cons-array 256));
(defvar defined_ascii_char_hash)
(define (initialize_ascii_char_hash)
(let ((char 0))
      (while (<= char 255)
        (hset ascii_char_hash (string-append (format nil "%c" char)) char)
        (set! char (+ char 1)))
(set! defined_ascii_char_hash t)
))

;; Convert a char into ascii value using above initialised table
(define (ascii_of char)
(if (not defined_ascii_char_hash) (initialize_ascii_char_hash))
      (href ascii_char_hash char)
)

;; A utility function for looking up text in the tables
(define (marathi_match_string_start_with_list input_string string_list)
  ;; Checks if any of the strings in string_list match the start of input_string
  ;; Returns the matched string from string_list
  (let (substr (matched_substr ""))
  (while (and (not (null? string_list)) (string-equal matched_substr ""))
  	(set! substr (car string_list))
	(set! string_list (cdr string_list))
	(if (string-equal substr "$") 
	  	(set! match_string (string-append "\\" substr ".*"))
	  	(set! match_string (string-append substr ".*")))
  	(if (string-matches input_string match_string) (set! matched_substr substr)))
  matched_substr
)) 

;; Cleans up the string to discard any charater that we are not going to handle
;; What we are going to handle is ascii, Devanagari unicode block, special symbols like euro etc.
(define (marathi_string_cleanup input_string)
  ;; Ensures the string has characters with are either in the marathi unicode range,
  ;; ascii punctuation or represent a currency symbol
(let (curr_char next_char next_next_char (clean_string "") matched_string)
  ;; Convert Devanagari unicode digits to ascii
  (while (and (set! matched_string (marathi_match_string_with_table input_string marathi_numbers_table))
	      (not (string-equal matched_string "")))
	(set! input_string (string-append (string-before input_string matched_string)
  	    (car (marathi_table_lookup matched_string marathi_numbers_table)) (string-after input_string matched_string))))
    ;; Iterate over all the characters in the string
    ;; We also need 2 look ahead characters for dirty handling for UTF-8 text
    (while (not (equal? input_string ""))
	(set! curr_char (ascii_of (substring input_string 0 1)))
	(set! next_char (ascii_of (substring input_string 1 1)))
	(set! next_next_char (ascii_of (substring input_string 2 1)))
	(cond
      	     ;; Basic ascii
	     ((or (or (and (>= curr_char 33) (<= curr_char 64)) (and (>= curr_char 91) (<= curr_char 96)))
		  (and (>= curr_char 123) (<= curr_char 126)))
	          (set! num_chars_valid 1))
             ;; FIXME: bring this part out so that code be separate from language specific knowledge
	     ;; Marathi unicode block (of the form \340 {\244 \245} [\200 .. \277]) 
	     ((and (equal? curr_char 224)
		           (or (equal? next_char 164) (equal? next_char 165))
		          (and (>= next_next_char 128) (<= next_next_char 191)))
	          	  (set! num_chars_valid 3))
	     ;; Supplementary chars like yen, pound, cent, copyright, registered mark, Euro
	     ((not (string-equal (set! matched_string 
		   (marathi_match_string_start_with_list input_string marathi_supplementary_char_list)) ""))
  	           (set! num_chars_valid (string-length matched_string)))
	     ;; Does not match anything, throw away the char
	     (t (set! num_chars_valid 0))
    	)
	(set! clean_string (string-append clean_string (substring input_string 0 num_chars_valid)))
	;; Does not match anything, throw away the char
        (if (equal? num_chars_valid 0) (set! num_chars_valid 1))
	(set! input_string (substring input_string num_chars_valid (- (string-length input_string) num_chars_valid))))
    clean_string
))

;; A utility function for looking up text in the tables
(define (marathi_match_string_with_table input_string string_table)
  ;; Checks is any of the strings in string_table match a substring of input_string
  ;; Returns the matched string from string_table
  (let (substr (matched_substr ""))
  (while (and (not (null? string_table)) (string-equal matched_substr ""))
  	(set! substr (car (car string_table)))
	(set! string_table (cdr string_table))
	(if (or (string-equal substr "$") (string-equal substr "^") (string-equal substr "?")
		(string-equal substr "*") (string-equal substr "+") (string-equal substr "."))
	  	(set! match_string (string-append ".*\\" substr ".*"))
	  	(set! match_string (string-append ".*" substr ".*")))
  	(if (string-matches input_string match_string) (set! matched_substr substr)))
  matched_substr
)) 

;; Most of the work happens here. From here we call all the other functions in the file
;; Called by marathi_tokne_to_words
(define (marathi_token_to_words_ token name)
  (set! name (marathi_string_cleanup name))

  ;; Numbers are to be matched a lot place and they contain commas and decimals
  (let ((number_regex "[0-9,]+\\(\\.[0-9]+\\)?") matched_substr matched_start_string prev_token_matched_start_string
						 currency_name time_segments date_segments)

  (set! matched_start_string (marathi_match_string_start_with_list name marathi_currency_list))
  (cond

    ;; FIXME: rewrite this so that code be separate from language specific knowledge
    ;; FIXME: the message translation can be generalised using $1 $2 etc. inside the strings

    ;; Currencies
    ;; When currencies are followed by billions etc., we need to change the order
    ;; Rs. 1000 Billion will become 1000 billion rupees
    ((and (not (string-equal matched_start_string ""))
	 (string-matches (string-after name matched_start_string) number_regex))
      (set! currency_name (string-append (car (marathi_table_lookup matched_start_string marathi_common_symbols_table)) ""))
      (if (marathi_list_lookup (item.feat token "n.name") marathi_denominations_list)
	  (marathi_token_to_words_ token (substring name 1  (- (string-length name) 1)))
	  (list (marathi_token_to_words_ token (substring name 1  (- (string-length name) 1))) currency_name)))
    ((and (not (string-equal (set! prev_token_matched_start_string (marathi_match_string_start_with_list 
			(item.feat token "p.name") marathi_currency_list)) ""))
       (string-matches (string-after (item.feat token "p.name") prev_token_matched_start_string) number_regex)
       (marathi_list_lookup name marathi_denominations_list)) 
           (list name (string-append 
			(car (marathi_table_lookup prev_token_matched_start_string marathi_common_symbols_table)) "")))

    ;; Rupees
    ((string-matches name (string-append "रू\\.[" number_regex))
        (if (marathi_list_lookup (item.feat token "n.name") marathi_denominations_list)
	  (marathi_token_to_words_ token (string-after name "रू."))
	  (list (marathi_token_to_words_ token (string-after name "रू.")) "रूपए")))
    ((and (string-matches (item.feat token "p.name") (string-append "रू\\.[" number_regex))
	          (marathi_list_lookup name marathi_denominations_list) ) (list name "रूपए"))

    ;; Cents - the symbol occurs after the number
    ((string-matches name (string-append number_regex "¢"))
        (list (marathi_token_to_words_ token (string-before name "¢")) "सेंट्स")) ;;  Cents

    ;; Simple numbers
    ((string-matches name "[0-9]+") (marathi_number_to_words name))
    ;; Numbers with decimal point
    ((string-matches name "[0-9]*\\.[0-9]+") 
        (list (marathi_number_to_words (string-before name ".")) '("पुर्णांक") ;; Point
		    (mapcar marathi_number_to_words (mapcar string-append (symbolexplode (string-after name "."))))))
    ;; Numbers of the form dd,dd,ddd.dd
    ((string-matches name "\\([0-9]+,[0-9]*\\)+\\(\.[0-9]+\\)?") 
		     (marathi_token_to_words_ token (marathi_removechar name ",")))
    ;; Numbers with a minus in the front
    ((string-matches name (string-append "-" number_regex)) (list '("उणे") ;; Minus
							    (marathi_token_to_words_ token (string-after name "-"))))
    ;; Numbers with a plus in the front
    ((string-matches name (string-append "\\+" number_regex)) (list '("अधिक") ;; adhika - Plus
							    (marathi_token_to_words_ token (string-after name "+")))) 

    ;; Line of characters
    ((string-matches name "_____+") (list '"अंडर" "स्कोरकी" "रेषा")) ;; A line of under scores
    ((string-matches name "=====+") (list "इक्वलटू" "की" "रेषा")) ;;  FIXME: unnecessary splits
    ((string-matches name "-----+") (list "हायफन्सकी" "रेषा")) ;; A line of hyphens, FIXME: unnecessary splits
    ((string-matches name "\\*\\*\\*\\*\\*+") (list "अॅस्टरिस्क्सकी" "रेषा")) ;;  : a line of asterisks

    ;; Time and Date
    ((set! time_segments (marathi_string_matches_time name)) (mapcar marathi_token_to_words_ 
						  	  (list token token token) (mapcar string-append time_segments)))
    ((set! date_segments (marathi_string_matches_date name)) (mapcar marathi_token_to_words_ 
						  	  (list token token token) (mapcar string-append date_segments)))

    ;; Abbreviations
    ;; Dotted abbreviations when followed by space ex: "Dr. Hello"
    ((and (marathi_table_lookup name marathi_dotted_abbr_list)  (string-equal (item.feat token "punc") "."))
		(marathi_table_lookup name marathi_dotted_abbr_list))

    ;; Abbreviation follwed by markers .,:,devanagari danda, double danda 
    ((and (set! matched_substr (marathi_match_string_with_table name marathi_abbr_markers_table))
	  (not (string-equal matched_substr ""))
     	  (marathi_table_lookup (string-before name matched_substr) marathi_dotted_abbr_list))
       	     (list (marathi_table_lookup (string-before name matched_substr) marathi_dotted_abbr_list)
	           (marathi_token_to_words_ token (string-after name matched_substr))))   

    ;; Abbreviations not followed by .
    ((marathi_table_lookup name marathi_abbr_list) (marathi_table_lookup name marathi_abbr_list))

    ;; Number followed by percentages
    ((string-matches name (string-append number_regex "%")) 
     		(list (marathi_token_to_words_ token (string-before name "%")) '("टक्के"))) ;; Takke - Percentage
    ;; Separators and connectors {#,$,%,&,*,+,-,/,<,=,>,@,\,^,_,`,copyright,registered trademark} 
    ((and (set! matched_substr (marathi_match_string_with_table name marathi_common_symbols_table) )
	  (not (string-equal matched_substr "")))
     	(list (marathi_token_to_words_ token (string-before name matched_substr))
	      (marathi_table_lookup matched_substr marathi_common_symbols_table)
	      (marathi_token_to_words_ token (string-after name matched_substr))))   

    ;; Nothing else todo, return
    (t (if (lts.in.alphabet (marathi_string_cleanup name) 'marathi) (list (marathi_string_cleanup name)) (list '(""))))
)))

;; Time matching function
(define (marathi_string_matches_time input_string)
  ;; Returns input string is in HH:MM(:SS) format
  (let ((hrs (parse-number (string-before input_string ":"))) mins secs)
  (cond 
    ((not (string-matches input_string "[0-9][0-9]?:[0-9][0-9]\\(:[0-9][0-9]\\)?")) nil)
    (t
    (set! input_string (string-after input_string ":"))
    (set! mins (string-before input_string ":"))
    (if (string-equal mins "") (set! mins (parse-number input_string)) (set! mins (parse-number mins)))
    (set! secs (string-after input_string ":"))

    ;; Checking for HH,MM,SS to be in valid 24 hour format
    (if (and (>= hrs 0) (<= hrs 23) (>= mins 0) (<= mins 59) (>= (parse-number secs) 0) (<= (parse-number secs) 59))
      		(list hrs mins secs) nil)
    )
)))

;; Storing the number of days in a month
;; so we can check for a valid date
(defvar marathi_no_of_days_in_month
  '( (1 31) (2 29)(3 31) (4 30)
     (5 31) (6 30) (7 31) (8 31)
     (9 30) (10 31) (11 30) (12 31)))

;; Date matching function
(define (marathi_string_matches_date input_string)
  ;; Returns true is the input string is in DD/MM(/YY/YYYY) format,
  (let ((date_segment1 (parse-number (string-before input_string "/"))) date_segment2 date_segment3 days_in_month)
  (cond
    ((not (string-matches input_string "[0-9][0-9]?/[0-9][0-9]?\\(/[0-9][0-9]\\([0-9][0-9]\\)?\\)?")) nil)
    (t
    (set! input_string (string-after input_string "/"))
    (set! date_segment2 (string-before input_string "/"))
    (if (string-equal date_segment2 "") (set! date_segment2 (parse-number input_string))
      					(set! date_segment2 (parse-number date_segment2)))
    (set! date_segment3 (string-after input_string "/"))
    ;; Checking for DD,MM to be in valid 
    (set! days_in_month (car (marathi_table_lookup date_segment2 marathi_no_of_days_in_month)))
    (if (and days_in_month (> date_segment1 0) (<= date_segment1 days_in_month)) 
      				(list date_segment1 date_segment2 date_segment3) nil))
)))

;; Convert numbers in the string to words in the language
;; Inturn calls another function
(define (marathi_number_to_words number)
  (let (input_string)
    (flatten (marathi_number_to_words_rec input_string number))
))

;; Convert numbers in the string to words in the language
(define (marathi_number_to_words_rec token_list name)
  ;; FIXME: rewrite so that code be separate from language specific knowledge
  (set! name (marathi_strip_leading_zeros name)) ;; Remove leading zeros
  (let ((number_length (string-length name)))
  (cond
    ;; If number is in crores we handle it here
    ((>= number_length 8)
     ;; Special case for 1 crore
     (if (string-equal name "10000000") (append token_list '("करोड़")) ;;  Crore
       ;; Case for round figured crores like 2 crores and 23 crores
       (if (string-matches name "[0-9]+0000000")
	 (append (marathi_number_to_words_rec token_list (substring name 0 (- number_length 7))) '("करोड़")) ;;  crore 
         ;;Case for one crore plus change
	 (if (string-matches name "1[0-9][0-9][0-9][0-9][0-9][0-9][0-9]") 
	     (append  token_list '("एक" "करोड़") (marathi_number_to_words_rec token_list (substring name (- number_length 7) 7))) ;; One crore 12987600, 10099887, etc
             ;; Default case if the number is other than above
	     (append (marathi_number_to_words_rec token_list (substring name 0 (- number_length 7))) '("करोड़") 
	           (marathi_number_to_words_rec token_list (substring name (- number_length 7) 7))))))) 
    ;; If number is in lakhs we handle it here
    ((and (<= number_length 7) (>= number_length 6))
     ;; Special case for 1 lakh
     (if (string-equal name "100000") (append token_list '("लाख")) ;; Lakh
       ;; Case for round figured lakhs like 2 lakhs and 56 lakhs
       (if (string-matches name "[0-9]+00000")
	 	(append token_list (marathi_two_digit_number_to_words (substring name 0 (- number_length 5))) '("लाख")) ;; lakhs 200000, 500000, etc
                ;;Case for one lakh plus change
		(if (string-matches name "1[0-9][0-9][0-9][0-9][0-9]")
        	   (marathi_number_to_words_rec (append token_list '("एक" "लाख")) (substring name (- number_length 5) 5)) ;; One laksha 198734, 178900, etc
                   ;; Default case if the number is other than above
	           (marathi_number_to_words_rec (append token_list (marathi_two_digit_number_to_words
			(substring name 0 (- number_length 5))) '("लाख")) (substring name (- number_length 5) 5)))))) 
    ;; If number is in thousands we handle it here
    ((and (<= number_length 5) (>= number_length 4)) 
     ;; Special case for 1 thousand
     (if (string-equal name "1000") (append token_list '("हजार")) ;;  Thousand
       ;; Case for round figured thousands like 2 thousand and 56 thousand
       (if (string-matches name "[0-9]+000")
	 	(append token_list (marathi_two_digit_number_to_words (substring name 0 (- number_length 3))) '("हजार")) ;; Thousands 2000, 7000, 3000, etc
         ;; Case for 4 digit numbers like 1100, 1200, 2300 
     	 (if (string-matches name "[0-9][1-9]00")
	   	(append token_list (marathi_two_digit_number_to_words 
				     (substring name 0 (- number_length 2))) '("सौ")) ;;  hundreds 2800, 1100, 8900, etc
                ;; Case for one thousand plus change
		(if (string-matches name "1[0-9][0-9][0-9]")
	           (marathi_number_to_words_rec (append token_list (marathi_two_digit_number_to_words 
			(substring name 0 (- number_length 3))) '("हजार")) (substring name (- number_length 3) 3)) ;;  thousand
;;                   (marathi_number_to_words_rec (append (marathi_two_digit_number_to_words (substring name 0 2))
;;						token_list '("सौ"))  (substring name 2 2)) ;; hundreds 1234, 5673, 9009, etc
                   ;; Default case if the number is other than above
	           (marathi_number_to_words_rec (append token_list (marathi_two_digit_number_to_words 
			(substring name 0 (- number_length 3))) '("हजार")) (substring name (- number_length 3) 3))))))) ;;  thousand
    ;; If number is less than thousand, we handle it here
    ((eq number_length 3)
     ;; Special case for one hundred
     (if (string-equal name "100") (append token_list '("एक" "सौ"))	;; - Hundred rupees
        ;; Case for round figured hundreds like 2 hundred and 9 hundred
        (if (string-matches name "[2-9]00")
	   (append token_list (marathi_two_digit_number_to_words 
				(substring name 0 (- number_length 2))) '("सौ")) ;;  hundred 200, 300, 400, etc
           ;; Case for one hundred plus change
           (if (string-matches name "1[0-9][0-9]") (append token_list '("एक" "सौ") ;; Hundred and.. 111, 109, 178, etc
					        	(marathi_two_digit_number_to_words (substring name 1 2)))
            ;; Default case if the number is other than above
	    (marathi_number_to_words_rec (append token_list (marathi_two_digit_number_to_words 
			(substring name 0 (- number_length 2))) '("सौ")) (substring name (- number_length 2) 2)))))) ;; hundred 987, 290, etc
    ;; If the number is less than hundred we call as function to handle it
    ((<= number_length 2) (append token_list (marathi_two_digit_number_to_words name)))
    ((= number_length 1) (append token_list (marathi_number_to_words name)))
)))

;; This table is queried by the two digit number to word converter
;; for coverting the units place
(defvar marathi_basic_number_table
  '(
    ("0" "शून्य") ;; Zero
    ("1" "एक") ;; One
    ("2" "दो") ;; Two
    ("3" "तीन") ;; Three
    ("4" "चार") ;; Four
    ("5" "पाँच") ;; Five
    ("6" "छे") ;; Six
    ("7" "सात") ;; Seven
    ("8" "आठ") ;; Eight
    ("9" "नौ") ;; Nine
))

;; This table is queried by the two digit number to word converter
;; for coverting the tens place and special numbers from 11 to 19
(defvar marathi_two_digit_numbers_table
  '(
    ("10" "दस") ;;  Ten
    ("11" "ग्यारह") ;;  Eleven
    ("12" "बारह") ;;  Twelve
    ("13" "तेरह") ;;  Thirteen
    ("14" "चौदह") ;;  Fourteen
    ("15" "पन्द्रह") ;; Fifteen
    ("16" "सोलह") ;; Sixteen
    ("17" "सतरह") ;; Seventeen
    ("18" "अठारह") ;; Eighteen
    ("19" "उन्नीस") ;; Nineteen
    ("20" "बीस") ;; Twenty

    ("21" "इक्कीस")
    ("22" "बाईस")
    ("23" "तेईस")
    ("24" "चौबीस")
    ("25" "पच्चीस")
    ("26" "छब्बीस")
    ("27" "सत्ताईस")
    ("28" "अठांईस")
    ("29" "उनतीस")
    ("30" "तीस") ;;  Thirty

    ("31" "इकतीस")
    ("32" "बत्तीस")
    ("33" "तैंतीस")
    ("34" "चौंतीस")
    ("35" "पैंतीस")
    ("36" "छत्तीस")
    ("37" "सैंतीस")
    ("38" "अड़तीस")
    ("39" "उनचालीस")
    ("40" "चालीस") ;;  Fourty

    ("41" "इकतालीस")
    ("42" "बयालीस")
    ("43" "तितालीस")
    ("44" "चौवालीस")
    ("45" "पैंतालीस")
    ("46" "छियालीस")
    ("47" "सैंतालीस")
    ("48" "अड़तालीस")
    ("49" "उनचास")
    ("50" "पचास") ;;  Fifty

    ("51" "इकावन")
    ("52" "बावन")
    ("53" "त्रेपन")
    ("54" "चौवन")
    ("55" "पचपन")
    ("56" "छप्पन")
    ("57" "सत्तावन")
    ("58" "अठावन")
    ("59" "उनसठ")
    ("60" "साठ") ;;  Sixty

    ("61" "एकसठ")
    ("62" "बासठ")
    ("63" "तिरसठ")
    ("64" "चौंसठ")
    ("65" "पैंसठ")
    ("66" "छियासठ")
    ("67" "सरसठ")
    ("68" "अड़सठ")
    ("69" "उनहतर")
    ("70" "सत्तर") ;;  Seventy

    ("71" "इकत्तर")
    ("72" "बहत्तर")
    ("73" "तिहत्तर")
    ("74" "चौहत्तर")
    ("75" "पचहत्तर")
    ("76" "छिहत्तर")
    ("77" "सतहत्तर")
    ("78" "अठहत्तर")
    ("79" "उन्नासी")
    ("80" "अस्सी") ;; Eighty

    ("81" "इकासी")
    ("82" "बयासी")
    ("83" "तिरासी")
    ("84" "चौरासी")
    ("85" "पचासी")
    ("86" "छियासी")
    ("87" "सत्तासी")
    ("88" "अठासी")
    ("89" "नवासी")
    ("90" "नब्बे") ;;  Ninety

    ("91" "इकानवे")
    ("92" "बानवे")
    ("93" "तिरानवे")
    ("94" "चौरानवे")
    ("95" "पंचानवे")
    ("96" "छियानवे")
    ("97" "सत्तानवे")
    ("98" "अठानवे")
    ("99" "निन्यानवे")
))

;; Utility function for list lookup
(define (marathi_list_lookup abbr lst)
 (assoc_string abbr lst))

;; Utility function for table lookup
(define (marathi_table_lookup abbr table)
  (cdr (assoc_string abbr table)))

;; Convert two digit numbers to words
;; Used by number to words function
;; Takes non zero numers
(define (marathi_two_digit_number_to_words name)
  ;; Number to words for 0-99
  ;; FIXME: is this language dependent?
  (let (lst units tens) 
    (set! lst (reverse (symbolexplode name)))
    (set! units (car lst))
    (set! tens (car (cdr lst)))
    ;; Supress the units digit if it is 0
    (if (and (string-equal units "0") (not (or (eq tens nil) (string-equal tens "1")))) (set! units "")) 
    ;; Remove leading zero
    (if (string-equal tens "0") (set! tens "")) 
    (if (string-matches tens "[1-9]") (marathi_table_lookup name marathi_two_digit_numbers_table)
	(cons (marathi_table_lookup tens marathi_two_digit_numbers_table) (marathi_table_lookup units marathi_basic_number_table))

 	;; above line is valid till "2 digit numbers to words" are english alike viz. 21-twenty-one, 95-ninety-five etc, hence not applicable to all languages like in marathi 21-ekavIsa (२१-एकवीस) etc.
;;	(marathi_table_lookup name marathi_two_digit_numbers_table)
;;	(marathi_table_lookup name marathi_basic_number_table)
)))

;; Used by number for words before any processing to remove
;; the leading zeros
(define (marathi_strip_leading_zeros number)
  ;; Removes leading zeros for a number, if single zero, leave it as it is
  (if (string-matches number "0+") (set! number "0")
       (while (string-matches number "0[0-9]*") (set! number (string-after number "0"))))
  number
)

;; Utility function to remove a particular charater from a string
(define (marathi_removechar input_string char)
  ;; Removes all occurences of char from input_string
  (let ((has_matched 1) match_string)
  (if (string-equal char "\\") (set! match_string (string-append ".*\\\\.*"))
      				 (set! match_string (string-append ".*" char ".*")))
  (while has_matched
    (if (string-matches input_string match_string)
	 (set! input_string (string-append (string-before input_string char) (string-after input_string char)))
	 (set! has_matched nil)))
    input_string))

(provide 'marathi_token)
