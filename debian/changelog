festival-mr (0.1-12) UNRELEASED; urgency=low

  [ Debian Janitor ]
  * Apply hints suggested by the multi-arch hinter.

 -- Kartik Mistry <kartik@debian.org>  Sat, 19 Nov 2022 16:59:26 +0530

festival-mr (0.1-11) unstable; urgency=low

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + festvox-mr-nsk: Add Multi-Arch: foreign.
  * Remove constraints unnecessary since buster:
    + festival-mr: Drop versioned constraint on festival in Depends.

  [ Kartik Mistry ]
  * debian/control:
    + Updated Standards-Version to 4.6.1
  * Updated debian/copyright.

 -- Kartik Mistry <kartik@debian.org>  Tue, 19 Jul 2022 11:35:20 +0530

festival-mr (0.1-10) unstable; urgency=low

  * debian/control:
    + Updated Maintainer/Uploaders.
    + Updated Vcs-* URLs.
    + Removed obsolete Homepage.
    + Switched to debhelper-compat.
    + Updated Standards-Version to 4.5.0
    + Removed Pre-Depends field.
    + Added Rules-Requires-Root field.
  * debian/rules:
    + Do not set custom compression.
  * Updated debian/copyright.
  * Added debian/gitlab-ci.yml.
  * Added version in dummy debian/watch file.

 -- Kartik Mistry <kartik@debian.org>  Wed, 06 May 2020 09:28:30 +0530

festival-mr (0.1-9) unstable; urgency=low

  * Team Upload.
  * debian/control:
    + Added Pre-Depends on dpkg (>= 1.15.6~) to introduce xz compression
  * debian/rules:
    + Introduced xz compression

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 23 Jun 2012 21:35:31 +0530

festival-mr (0.1-8) unstable; urgency=low

  * Team upload.
  * debian/control:
    + Updated debhelper version to 8.0.0
    + Updated Standards-Version to 3.9.3 (no changes needed)
    + Update the Vcs fields to Git
  * debian/copyright:
    + Updated debian/copyright to copyright-format 1.0

 -- AbdulKarim Memon <abdulkarimmemon@gmail.com>  Sat, 03 Mar 2012 15:55:28 +0530

festival-mr (0.1-7) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.9.2
    + Updated debhelper dependency
    + Wrap-up descriptions
  * debian/rules:
    + Use dh for simple rules file
  * debian/copyright:
    + Use DEP-5 format

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Thu, 04 Aug 2011 13:03:43 +0530

festival-mr (0.1-6) unstable; urgency=low

  [Kartik Mistry]
  * Converted package to use source format 3.0 (quilt)
  * debian/control:
    + Updated Standards-Version to 3.8.4
    + Added ${misc:Depends} to binary packages
  * debian/rules:
    + Used dh_prep instead of dh_clean -k
  * debian/copyright:
    + Do not use versionless symlinks to license texts
    + Updated Debian package copyright year

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Tue, 02 Feb 2010 10:56:19 +0530

festival-mr (0.1-5) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated Standards-Version to 3.8.2
    + Updated my maintainer email address
  * debian/copyright:
    + Updated copyright to use correct copyright symbol
    + Updated Debian package copyright information
  * debian/rules:
    + Wrapped long lines to make it readable
  * debian/watch:
    + Added dummy watch file as per DEHS
  * debian/*.dirs:
    + Removed duplicate entries

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 24 Jun 2009 11:51:54 +0530

festival-mr (0.1-4) unstable; urgency=low

  [Kartik Mistry]
  * debian/control:
    + Updated package description
    + Updated Homepage entry
    + Updated Standards-Version to 3.7.3
    + Added VCS-* fields

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 26 Jan 2008 15:04:56 +0530

festival-mr (0.1-3) unstable; urgency=low

  [Kartik Mistry]
  * debian/rules: fixed lintian warning of example/sample.txt's excutable mode
  * debian/copyright: updated according to standard copyright file

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Sat, 28 Apr 2007 11:54:55 +0530

festival-mr (0.1-2) unstable; urgency=low

  [Kartik Mistry]
  * Changed maintainer address to Debian-IN Team

 -- Debian-IN Team <debian-in-workers@lists.alioth.debian.org>  Wed, 21 Mar 2007 13:27:30 +0530

festival-mr (0.1-1) unstable; urgency=low

  * Initial release (Closes: #405785)

 -- Kartik Mistry <kartik.mistry@gmail.com>  Sat,  6 Jan 2007 10:42:46 +0530
