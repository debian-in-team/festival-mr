#!/bin/bash

#################################################################################
##									       ##
##             Installation script for festival-mr-0.1			       ##
##									       ##
##  Copyright (c) 2006, Priti Patil, janabhaaratii, C-DAC, Mumbai 	       ##
##                      <prithisd@cdacmumbai.in>, <prithisd@gmail.com>         ##
##                                                                             ##
##  Copyright (c) 2005, Chaitanya Kamisetty <chaitanya@atc.tcs.co.in>          ## 
##									       ##
##  This script is a part of festival-mr (modified version of festival-te).    ##
##  									       ##
##  festival-mr is free software; you can redistribute it and/or modify        ##
##  it under the terms of the GNU General Public License as published by       ##
##  the Free Software Foundation; either version 2 of the License, or	       ##
##  (at your option) any later version.					       ##
##									       ##
##  This program is distributed in the hope that it will be useful,	       ##
##  but WITHOUT ANY WARRANTY; without even the implied warranty of	       ##
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the	       ##
##  GNU General Public License for more details.			       ##
##									       ##
##  You should have received a copy of the GNU General Public License	       ##
##  along with this program; if not, write to the Free Software		       ##
##  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA  ##
##									       ##
#################################################################################


if [ $1 ]; then
	FESTIVAL_DIR=$1
else
        FESTIVAL_DIR=/usr/share/festival
fi

#Checking for festival installation
echo -n "Checking for festival at $FESTIVAL_DIR............... "
if [ -d $FESTIVAL_DIR ]; then 
	echo "OK"
else
	echo
	echo "festival not found at $FESTIVAL_DIR"
	echo
	echo "Please note that you need festival for festival-mr to run."
	echo "If your festival package is installed at a different location" 
	echo "you need to specify the festival directory to this installation script as follows:"
	echo 
	echo "Usage: $0 FESTIVAL_DIR"
	echo "Example: ./install.sh /usr/local/share/festival"
	exit
fi

#Checking for festival lib/ directory
if [ -d "$FESTIVAL_DIR/lib/" ]; then
	FESTIVAL_LIB_DIR="$FESTIVAL_DIR/lib/"
else
	FESTIVAL_LIB_DIR=$FESTIVAL_DIR
fi

#Checking for festival voices/ directory
if [ -d "$FESTIVAL_DIR/lib/voices" ]; then
	FESTIVAL_VOICES_DIR="$FESTIVAL_DIR/lib/voices/"
else
	FESTIVAL_VOICES_DIR="$FESTIVAL_DIR/voices"
fi

# Installing festival-mr
echo -n "Installing festival-mr.............. "
install -D -m 644 marathi_scm/marathi_lex.scm $FESTIVAL_LIB_DIR/marathi_scm/marathi_lex.scm
install -D -m 644 marathi_scm/marathi_NSK_int.scm $FESTIVAL_LIB_DIR/marathi_scm/marathi_NSK_int.scm
install -D -m 644 marathi_scm/marathi_phones.scm $FESTIVAL_LIB_DIR/marathi_scm/marathi_phones.scm
install -D -m 644 marathi_scm/marathi_token.scm $FESTIVAL_LIB_DIR/marathi_scm/marathi_token.scm
install -D -m 644 marathi_NSK_diphone/festvox/marathi_NSK_diphone.scm \
			$FESTIVAL_VOICES_DIR/marathi/marathi_NSK_diphone/festvox/marathi_NSK_diphone.scm  
install -D -m 644 marathi_NSK_diphone/festvox/marathi_NSK_dur.scm \
			$FESTIVAL_VOICES_DIR/marathi/marathi_NSK_diphone/festvox/marathi_NSK_dur.scm
install -D -m 644 marathi_NSK_diphone/festvox/marathi_NSK_ene.scm \
			$FESTIVAL_VOICES_DIR/marathi/marathi_NSK_diphone/festvox/marathi_NSK_ene.scm
install -D -m 644 marathi_NSK_diphone/group/NSKlpc.group \
			$FESTIVAL_VOICES_DIR/marathi/marathi_NSK_diphone/group/NSKlpc.group
echo "DONE"
echo 
echo "If you are using the festival command line, select Marathi voice using \"(voice_marathi_NSK_diphone)\""
